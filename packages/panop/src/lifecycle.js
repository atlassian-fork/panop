// @flow weak

import fs from 'fs';
import path from 'path';
import { sortSemverDescending } from './utils';
import * as progress from './progress';
import {
  groupTagsForComponents,
  calculateLatestPublishedVersion,
  mergeDataForTag,
  parseComponentTag,
} from './lifecycle_helpers';

const tagRegex = /^(@[a-z-]+\/)?[a-z-]+@[0-9.]+$/;

export default class Lifecycle {
  plugins: Array<any>;
  git: Object;
  processComponent: Function;
  processComponentTag: Function;

  constructor(opts) {
    this.plugins = opts.plugins;
    this.git = opts.git;

    this.processComponent = this.processComponent.bind(this);
    this.processComponentTag = this.processComponentTag.bind(this);
    this.getPackageNamesForDirectories = this.getPackageNamesForDirectories.bind(this);

    // TODO split into helper and handle errors
    if (opts.mergeData) {
      try {
        this.mergeData = JSON.parse(fs.readFileSync(path.resolve(opts.mergeData)));
      } catch (e) {
        console.error('Could not read merge data file. Continuing.'); // eslint-disable-line no-console
      }
    }
  }

  // returns a promise that resolves to array of component objects
  // with keys:
  // {
  //   "dirName": "util-cz-atlaskit-changelog",
  //   "pkgName": "@atlaskit/util-cz-atlaskit-changelog"
  // },
  async getComponents({ pkgScope, maxComponents }) {
    return this.git.bitbucketLs('packages', {
      subkey: 'directories',
    }).then((results) => {
      if (pkgScope) {
        return results.filter(r => r === pkgScope);
      } else if (maxComponents) {
        return results.slice(0, maxComponents);
      }
      return results;
    }).then(results => Promise.all(results.map(this.getPackageNamesForDirectories)));
  }

  // given an directory name string, returns an object { dirName, pkgName }
  getPackageNamesForDirectories(dirName) {
    return this.git.getFileContents({
      path: `packages/${dirName}/package.json`,
    }).then((pkgJson) => {
      const { name: pkgName } = pkgJson;
      return { dirName, pkgName };
    });
  }

  async addTagsToComponents(components) {
    return this.git.getTags().then(tags => groupTagsForComponents(components, tags));
  }

  async processComponents(input) {
    const { components, totalVersions } = input;

    progress.start(totalVersions * this.plugins.length);
    return Promise.all(
      components.map(this.processComponent)
    ).then(results => (
      results.filter(r => r.versions.length > 0)
    ));
  }

  processComponent(component) {
    const processTagForThisComponent = this.processComponentTag.bind(this, component);
    return Promise.all(
      component.tags
        .filter(tag => tagRegex.test(tag))
        .map(processTagForThisComponent)
    ).then((componentVersions) => {
      const sortedVersions = componentVersions.sort(sortSemverDescending);
      const latestPublishedVersion = calculateLatestPublishedVersion(sortedVersions);
      return {
        name: component.name,
        isPublished: !!latestPublishedVersion,
        latestPublishedVersion: latestPublishedVersion || 'None',
        versions: sortedVersions,
      };
    });
  }

  processComponentTag(component, tag) {
    const { scopedPackageName, packageName, packageVersion } = parseComponentTag(tag);
    const mergeVersion = mergeDataForTag(this.mergeData, packageName, packageVersion);
    if (mergeVersion) {
      progress.tick(this.plugins.length);
      return mergeVersion;
    }

    return this.getAllPluginData(scopedPackageName, packageName, tag).then((pluginResults) => {
      // pkgJson is first plugin TODO: calculate index dynamically
      const versionPkgJson = pluginResults[0];

      const versionInfo = {
        name: versionPkgJson.name,
        version: versionPkgJson.version,
      };

      this.plugins.forEach((plugin, pluginIndex) => {
        versionInfo[plugin.resultKey] = pluginResults[pluginIndex];
      });

      return versionInfo;
    });
  }

  getAllPluginData(scopedPackageName, packageName, tag) {
    const producePromise = plugin =>
      progress.promiseWithProgress(
        plugin.getData(scopedPackageName, packageName, tag, this.git)
      );
    return Promise.all(this.plugins.map(producePromise));
  }

}
