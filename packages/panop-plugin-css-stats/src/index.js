/* eslint-disable class-methods-use-this */
/* eslint-disable no-param-reassign */

import cssstats from 'cssstats';

function combinedStatsForCssFiles(cssFileContents) {
  let combinedStats = {};
  if (cssFileContents.length) {
    const separateStats = cssFileContents.map((css) => {
      const stats = cssstats(css);
      return {
        size: stats.size,
        gzipSize: stats.gzipSize,
        ruleCount: (stats.rules && stats.rules.total) || 0,
        selectorCount: (stats.selectors && stats.selectors.total) || 0,
        maxSelectorSpecificity: (stats.selectors && stats.selectors.specificity.max) || 0,
        averageSelectorSpecificity: (stats.selectors && stats.selectors.specificity.average) || 0,
        declarationCount: (stats.delcarations && stats.declarations.total) || 0,
      };
    });

    // Sum the stats that make sense to add (i.e. all except max/avg)
    const statKeysToSum = Object.keys(separateStats[0]);
    statKeysToSum.splice(statKeysToSum.indexOf('maxSelectorSpecificity'), 1);

    // statKeysToSum.splice(statKeysToSum.indexOf('averageSelectorSpecificity'), 1);
    combinedStats = separateStats.reduce((previousValue, currentValue) => {
      statKeysToSum.forEach((statKey) => {
        previousValue[statKey] += currentValue[statKey];
      });

      // Handle the maxSelectorSpecificity separately because it's not a sum operation
      if (currentValue.maxSelectorSpecificity > previousValue.maxSelectorSpecificity) {
        previousValue.maxSelectorSpecificity = currentValue.maxSelectorSpecificity;
      }

      return previousValue;
    });

    // Here we need to divide the total because it was only summed during the reduce op
    combinedStats.averageSelectorSpecificity = parseFloat(
      combinedStats.averageSelectorSpecificity / separateStats.length
    ).toFixed(2);
  }
  return combinedStats;
}

module.exports = class CssStats {

    // Must return unique non-empty string
  get resultKey() { return 'cssStats'; }

    // Must return promise
  getData(scopedPackageName, packageDirName, tag, bb) {
    return bb.getAllFileContents({
      extension: 'css|less',
      path: `packages/${packageDirName}/src`,
      tag,
      packageDirName,
    }).then(allJsContents => combinedStatsForCssFiles(allJsContents));
  }

};
