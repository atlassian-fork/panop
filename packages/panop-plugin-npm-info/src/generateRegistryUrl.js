export default function generateRegistryUrl(scopedPackageName) {
  return `https://registry.npmjs.org/${scopedPackageName.replace('/', '%2F')}`;
}
