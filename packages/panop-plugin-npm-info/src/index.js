/* eslint-disable class-methods-use-this */
/* eslint-disable no-param-reassign */
/* eslint-disable no-prototype-builtins */

import request from 'request';
import generateRegistryUrl from './generateRegistryUrl';

module.exports = class NpmInfoPlugin {

  // Must return unique non-empty string
  get resultKey() { return 'npmInfo'; }

  // Must return promise
  getData(scopedPackageName, packageDirName, tag) {
    return new Promise((resolve, reject) => {
      const wantedVersion = tag.split('@').pop();
      try {
        request({
          url: generateRegistryUrl(scopedPackageName),
          json: true,
        }, (error, response, body) => {
          if (error) {
            reject(error);
            return;
          }
          const repoExists = !!body.time;
          const isPublished = repoExists && body.time.hasOwnProperty(wantedVersion);
          const publishTime = isPublished && body.time[wantedVersion];
          resolve({ isPublished, publishTime });
        });
      } catch (e) {
        reject(e);
      }
    });
  }

};
