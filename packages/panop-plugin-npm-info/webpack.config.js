const baseConfig = require('../../webpack.config.js');

baseConfig.output.path = __dirname;
baseConfig.output.library = require('./package.json').name; // eslint-disable-line

module.exports = baseConfig;
