/* eslint-disable class-methods-use-this */
/* eslint-disable no-param-reassign */

function combinedStatsForJsFiles(jsFileContents) {
  return {
    size: jsFileContents.join('').length,
  };
}

module.exports = class JsStats {

  // Must return unique non-empty string
  get resultKey() { return 'jsStats'; }

  // Must return promise
  getData(scopedPackageName, packageDirName, tag, bb) {
    return bb.getAllFileContents({
      extension: 'js',
      path: `packages/${packageDirName}/src`,
      tag,
      packageDirName,
    }).then(allJsContents => combinedStatsForJsFiles(allJsContents));
  }

};
